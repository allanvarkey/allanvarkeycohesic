import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CohecisDropdownComponent } from './cohecis-dropdown.component';

describe('CohecisDropdownComponent', () => {
  let component: CohecisDropdownComponent;
  let fixture: ComponentFixture<CohecisDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CohecisDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CohecisDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
