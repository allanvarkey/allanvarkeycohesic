import { Component, OnInit } from '@angular/core';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';


@Component({
  selector: 'app-cohecis-dropdown',
  templateUrl: './cohecis-dropdown.component.html',
  styleUrls: ['./cohecis-dropdown.component.sass']
})
export class CohecisDropdownComponent implements OnInit {

  config = TreeviewConfig.create({
    hasAllCheckBox: false,
    hasFilter: false,
    hasCollapseExpand: false,
    decoupleChildFromParent: false,
    maxHeight: 500
  });
  items: TreeviewItem[];

  constructor() { }

  ngOnInit() {

    const coehsicDataTreeViewItem = new TreeviewItem(
      {
        text: 'Chest', value: 1, children: [
          {
            text: 'Lungs', value: 911, children: [
              {
                text: 'Right Lung', value: 9111, children: [
                  { text: 'Superior Lobe', value: 91111 },
                  { text: 'Middle Lobe', value: 91112 },
                  { text: 'Inferior Lobe', value: 91113 }
                ]
              },
              {
                text: 'Left Lung', value: 9112, children: [

                  { text: 'Superior Lobe', value: 91121 },
                  { text: 'Inferior Lobe', value: 91122 }
                ]
              },

            ]
          }, {
            text: 'Heart', value: 912, children: [
              { text: 'Left Ventricle', value: 9121 },
              { text: 'Right Ventricle', value: 9122 },
              { text: 'Left Atrium', value: 9123 },
              { text: 'Right Atrium', value: 9124 },
              { text: 'Septum', value: 9125 }
            ]
          }
        ] //end of chest Children
      }// End of treeview object
    ); // end of Tree View Item

    this.items = [coehsicDataTreeViewItem];

   
  }

}
