import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TreeviewModule } from 'ngx-treeview';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CohecisDropdownComponent } from './components/cohecis-dropdown/cohecis-dropdown.component';

@NgModule({
  declarations: [
    AppComponent,
    CohecisDropdownComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TreeviewModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
