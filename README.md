# CohesicSolution For Allan Varkey


Hello.

Thank you for giving the oportunity to take part in this coding exercise. 
in order to run the application please follow steps below.

Please download the code from the following repository
git clone https://allanvarkey@bitbucket.org/allanvarkey/allanvarkeycohesic.git

1.  Open a command prompt window and navigate to the solution folder. 
2.  In order to run the solution you will need node package manager installed.
        if its not installed install via `npm install npm@latest -g`
3.  Please install the dependices for the solution  using `npm install`.
4.  In order to run the application we run `ng serve --open`. This opens the solution in your default browser on port 4200.
        if you would like to specify a port you can run `ng serve --open --port=XXXX`
